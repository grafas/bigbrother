# README #

### Build and run

- Clone codebase to your enviroment `git clone git@bitbucket.org:grafas/bigbrother.git`
- Run `./gradlew installDebug`
- On your device locate and launch app "BigBrother"
- Follow on screen instructions

### General information

- using browser/Postman/other app or tool call url `ip_from_the_app/help` to find out information about the services provided by the app

