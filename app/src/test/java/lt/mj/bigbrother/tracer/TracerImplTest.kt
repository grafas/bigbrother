package lt.mj.bigbrother.tracer

import android.telephony.TelephonyManager.*
import org.junit.Assert.*
import org.junit.Test

/**
 * @author Martynas Jurkus
 */
class TracerImplTest {

    private val fixture: Tracer = TracerImpl()

    @Test
    fun new_outgoing() {
        fixture.newOutgoing(PHONE_NO_1)

        assertEquals(PHONE_NO_1, fixture.ongoingCall!!.number)
        assertFalse(fixture.ongoingCall!!.incoming)
        assertTrue(fixture.getCallLog().isEmpty())
    }

    @Test
    fun new_incoming() {
        fixture.onCallStateChanged(CALL_STATE_RINGING, PHONE_NO_2)

        assertEquals(PHONE_NO_2, fixture.ongoingCall!!.number)
        assertTrue(fixture.ongoingCall!!.incoming)
        assertTrue(fixture.getCallLog().isEmpty())
    }

    @Test
    fun start_end_call() {
        fixture.onCallStateChanged(CALL_STATE_RINGING, PHONE_NO_2)
        Thread.sleep(5)

        fixture.onCallStateChanged(CALL_STATE_OFFHOOK, PHONE_NO_2)
        Thread.sleep(5)

        fixture.onCallStateChanged(CALL_STATE_IDLE, PHONE_NO_2)

        assertNull(fixture.ongoingCall)
        val call = fixture.getCallLog().first()

        assertEquals(PHONE_NO_2, call.number)
        assertTrue(call.duration > 10)
    }

    companion object {
        private const val PHONE_NO_1 = "123456"
        private const val PHONE_NO_2 = "234567"
    }
}