package lt.mj.bigbrother.http.interactor

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import lt.mj.bigbrother.contacts.ContactData
import lt.mj.bigbrother.contacts.ContactDataProvider
import lt.mj.bigbrother.tracer.Call
import lt.mj.bigbrother.tracer.Tracer
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * @author Martynas Jurkus
 */
class TraceDataInteractorImplTest {

    private val tracer = mock<Tracer> {
        on { getCallLog() } doReturn (1..93).map { Call(number = "$it") }
    }
    private val contactDataProvider = mock<ContactDataProvider> {
        on { getContactData(any()) } doReturn emptyMap<String, ContactData>()
    }

    private val fixture: TraceDataInteractor = TraceDataInteractorImpl(
            host = "any",
            tracer = tracer,
            contactDataProvider = contactDataProvider
    )

    @Test
    fun request_log_10_per_page() {
        val response = fixture.logResponse(1, perPage = 10)

        assertEquals(10, response.entries.size)
        assertEquals("1", response.entries.first().number)
    }

    @Test
    fun request_log_second_page() {
        val response = fixture.logResponse(2, perPage = 10)

        assertEquals(10, response.entries.size)
        assertEquals("11", response.entries.first().number)
    }

    @Test
    fun request_log_last_page() {
        val response = fixture.logResponse(10, perPage = 10)

        assertEquals(3, response.entries.size)
        assertEquals("93", response.entries.last().number)
    }

    @Test
    fun request_log_verify_pagination() {
        val pagination = fixture.logResponse(2, perPage = 20).pagination

        assertEquals(2, pagination.page)
        assertEquals(20, pagination.perPage)
        assertEquals(93, pagination.totalEntries)
        assertEquals(5, pagination.totalPages)
    }
}