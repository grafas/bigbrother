package lt.mj.bigbrother

import android.app.Application
import lt.mj.bigbrother.dagger.ApplicationComponent
import lt.mj.bigbrother.dagger.DaggerApplicationComponent
import lt.mj.bigbrother.dagger.modules.ApplicationModule
import lt.mj.bigbrother.http.BigBrotherServer
import javax.inject.Inject

/**
 * @author Martynas Jurkus
 */
class BigBrotherApp : Application() {

    lateinit var component: ApplicationComponent

    @Inject
    lateinit var webServer: BigBrotherServer

    override fun onCreate() {
        super.onCreate()
        inject()
    }

    override fun onTerminate() {
        webServer.stop()
        super.onTerminate()
    }

    private fun inject() {
        component = buildComponent()
        component.inject(this)
    }

    private fun buildComponent(): ApplicationComponent {
        return DaggerApplicationComponent
                .builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }
}