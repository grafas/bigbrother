package lt.mj.bigbrother.contacts

import android.content.Context
import android.provider.ContactsContract
import android.provider.ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER
import android.provider.ContactsContract.CommonDataKinds.Phone.NUMBER
import android.provider.ContactsContract.Contacts
import org.jetbrains.anko.db.parseList
import org.jetbrains.anko.db.rowParser


/**
 * @author Martynas Jurkus
 */
class ContactDataProviderImpl(
        private val context: Context
) : ContactDataProvider {

    private val projection = arrayOf(Contacts.DISPLAY_NAME, NUMBER, NORMALIZED_NUMBER)

    private val contactDataParser = rowParser(::ContactProjection)

    override fun getContactData(numbers: List<String>): Map<String, ContactData> {
        return context.contentResolver.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                projection,
                "$NORMALIZED_NUMBER IN ( ${numbers.joinToString(",") { "?" }} )",
                numbers.toTypedArray(),
                null
        )
                .parseList(contactDataParser)
                .filterNot { it.normalized.isNullOrBlank() }
                .associate {
                    Pair(it.normalized!!, ContactData(it.name))
                }
    }

    private data class ContactProjection(
            val name: String?,
            val number: String?,
            val normalized: String?
    )
}