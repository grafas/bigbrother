package lt.mj.bigbrother.contacts

interface ContactDataProvider {

    fun getContactData(numbers: List<String>) : Map<String, ContactData>
}
