package lt.mj.bigbrother.contacts

/**
 * @author Martynas Jurkus
 */
data class ContactData(
        val name: String? = null
)