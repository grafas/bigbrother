package lt.mj.bigbrother.dagger

import dagger.Component
import lt.mj.bigbrother.BigBrotherApp
import lt.mj.bigbrother.MainActivity
import lt.mj.bigbrother.PhoneStateReceiver
import lt.mj.bigbrother.dagger.modules.ApplicationModule
import lt.mj.bigbrother.dagger.modules.ServerModule
import lt.mj.bigbrother.dagger.modules.TraceModule
import javax.inject.Singleton

/**
 * @author Martynas Jurkus
 */
@Singleton
@Component(modules = [
    ApplicationModule::class,
    TraceModule::class,
    ServerModule::class
])
interface ApplicationComponent {

    fun inject(application: BigBrotherApp)
    fun inject(phoneStateReceiver: PhoneStateReceiver)
    fun inject(activity: MainActivity)
}