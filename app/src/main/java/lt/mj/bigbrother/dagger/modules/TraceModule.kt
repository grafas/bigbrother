package lt.mj.bigbrother.dagger.modules

import dagger.Module
import dagger.Provides
import lt.mj.bigbrother.tracer.Tracer
import lt.mj.bigbrother.tracer.TracerImpl
import javax.inject.Singleton

/**
 * @author Martynas Jurkus
 */
@Module
class TraceModule {

    @Provides
    @Singleton
    fun provideCallTracer(): Tracer {
        return TracerImpl()
    }
}