package lt.mj.bigbrother.dagger.modules

import android.app.Application
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import lt.mj.bigbrother.contacts.ContactDataProvider
import lt.mj.bigbrother.contacts.ContactDataProviderImpl
import lt.mj.bigbrother.http.BigBrotherServer
import lt.mj.bigbrother.http.interactor.TraceDataInteractor
import lt.mj.bigbrother.http.interactor.TraceDataInteractorImpl
import lt.mj.bigbrother.tracer.Tracer
import java.net.Inet4Address
import java.net.NetworkInterface
import javax.inject.Named
import javax.inject.Singleton


/**
 * @author Martynas Jurkus
 */
@Module
class ServerModule {

    @Named("host")
    @Provides
    fun provideHost(): String {
        val en = NetworkInterface.getNetworkInterfaces()

        return en.asSequence()
                .map {
                    it.inetAddresses.asSequence()
                            .filterIsInstance(Inet4Address::class.java)
                            .filterNot { it.isLoopbackAddress }
                            .firstOrNull()
                }
                .filterNotNull()
                .first().hostAddress
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create()
    }

    @Provides
    @Singleton
    fun provideContactProvider(application: Application): ContactDataProvider {
        return ContactDataProviderImpl(application)
    }

    @Provides
    @Singleton
    fun provideWebServer(
            gson: Gson,
            interactor: TraceDataInteractor
    ): BigBrotherServer {
        return BigBrotherServer(PORT, gson, interactor)
    }

    @Provides
    fun provideInteractor(
            @Named("host") host: String,
            contactDataProvider: ContactDataProvider,
            tracer: Tracer
    ): TraceDataInteractor {
        return TraceDataInteractorImpl(
                "http://$host:$PORT",
                tracer,
                contactDataProvider
        )
    }

    companion object {
        private const val PORT = 8081
    }
}
