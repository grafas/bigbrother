package lt.mj.bigbrother.dagger.modules

import android.app.Application
import dagger.Module
import dagger.Provides

/**
 * @author Martynas Jurkus
 */
@Module
class ApplicationModule(
        val application: Application
) {

    @Provides
    fun provideApplication(): Application {
        return application
    }
}