package lt.mj.bigbrother.http.responses

import lt.mj.bigbrother.http.data.CallLog


/**
 * @author Martynas Jurkus
 */
data class LogResponse(
        val entries: List<CallLog>,
        val pagination: Pagination
)