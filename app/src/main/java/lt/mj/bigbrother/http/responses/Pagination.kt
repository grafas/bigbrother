package lt.mj.bigbrother.http.responses

/**
 * @author Martynas Jurkus
 */
data class Pagination(
        val page: Int = 0,
        val perPage: Int = 0,
        val totalPages: Int = 0,
        val totalEntries: Int = 0
)