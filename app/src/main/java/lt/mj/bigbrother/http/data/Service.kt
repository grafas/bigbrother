package lt.mj.bigbrother.http.data

/**
 * @author Martynas Jurkus
 */
data class Service(
        val name: String,
        val uri: String,
        val params: List<Param>
) {
    data class Param(
            val name: String,
            val type: String,
            val description: String
    )
}