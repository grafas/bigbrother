package lt.mj.bigbrother.http.responses

import lt.mj.bigbrother.http.data.CallStatus

/**
 * @author Martynas Jurkus
 */
sealed class StatusResponse(val status: String) {

    class OutgoingCall(val call: CallStatus) : StatusResponse("Outgoing")
    class IncomingCall(val call: CallStatus) : StatusResponse("Incoming")
    class Idle : StatusResponse("Idle")
}

