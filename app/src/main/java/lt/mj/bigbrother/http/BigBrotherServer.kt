package lt.mj.bigbrother.http

import com.google.gson.Gson
import fi.iki.elonen.NanoHTTPD
import lt.mj.bigbrother.http.interactor.TraceDataInteractor


/**
 * @author Martynas Jurkus
 */
class BigBrotherServer(
        port: Int,
        private val gson: Gson,
        private val interactor: TraceDataInteractor
) : NanoHTTPD(port) {

    override fun serve(session: IHTTPSession): Response {
        return when (session.uri) {
            "/status" -> serveStatus()
            "/log" -> serveLog(session)
            "/help" -> serveHelp()
            else -> super.serve(session)
        }
    }

    private fun serveLog(session: IHTTPSession): Response {
        return interactor.logResponse(
                session.parms["page"]?.toInt() ?: 1,
                session.parms["per_page"]?.toInt() ?: DEFAULT_PER_PAGE
        ).asResponse()
    }

    private fun serveHelp(): Response {
        return interactor.helpResponse().asResponse()
    }

    private fun serveStatus(): Response {
        return interactor.statusResponse().asResponse()
    }

    private fun Any.asResponse(): NanoHTTPD.Response {
        return NanoHTTPD.newFixedLengthResponse(
                NanoHTTPD.Response.Status.OK,
                MIME_TYPE_JSON,
                gson.toJson(this)
        )
    }

    companion object {
        private const val MIME_TYPE_JSON = "application/json"
        private const val DEFAULT_PER_PAGE = 20
    }
}

