package lt.mj.bigbrother.http.interactor

import lt.mj.bigbrother.contacts.ContactData
import lt.mj.bigbrother.contacts.ContactDataProvider
import lt.mj.bigbrother.err
import lt.mj.bigbrother.http.data.CallLog
import lt.mj.bigbrother.http.data.CallStatus
import lt.mj.bigbrother.http.data.Service
import lt.mj.bigbrother.http.responses.HelpResponse
import lt.mj.bigbrother.http.responses.LogResponse
import lt.mj.bigbrother.http.responses.Pagination
import lt.mj.bigbrother.http.responses.StatusResponse
import lt.mj.bigbrother.tracer.Call
import lt.mj.bigbrother.tracer.Tracer
import java.util.*

/**
 * @author Martynas Jurkus
 */
class TraceDataInteractorImpl(
        private val host: String,
        private val tracer: Tracer,
        private val contactDataProvider: ContactDataProvider
) : TraceDataInteractor {

    override fun helpResponse(): HelpResponse {
        return HelpResponse(
                services = listOf(
                        Service(
                                name = "status",
                                uri = "$host/status",
                                params = listOf(
                                        Service.Param(
                                                name = "page",
                                                type = "int",
                                                description = "Optional. Page to load"
                                        ),
                                        Service.Param(
                                                name = "per_page",
                                                type = "int",
                                                description = "Optional. Call logs per page"
                                        )
                                )
                        ),
                        Service(
                                name = "log",
                                uri = "$host/log",
                                params = emptyList()
                        )
                )
        )
    }

    override fun statusResponse(): StatusResponse {
        val call = tracer.ongoingCall

        return if (call == null) {
            StatusResponse.Idle()
        } else {
            tracer.incrementQueryCount()
            val contactData = fetchContactData(listOf(call))
            val calStatus = CallStatus(
                    number = call.number,
                    contactData = contactData[call.number]
            )

            if (call.incoming) {
                StatusResponse.IncomingCall(calStatus)
            } else {
                StatusResponse.OutgoingCall(calStatus)
            }
        }
    }

    override fun logResponse(
            page: Int,
            perPage: Int
    ): LogResponse {
        //poor man's pagination, but as long everything is in memory, then OK.
        val paginated = tracer.getCallLog().chunked(perPage)
        val entries = paginated
                .drop(page - 1)
                .firstOrNull()
                .orEmpty()

        val contactData = fetchContactData(entries)

        val merged = entries.map {
            CallLog(
                    number = it.number,
                    callStart = Date(it.callStart),
                    duration = it.duration,
                    contactData = contactData[it.number],
                    timesQueried = it.timesQueried
            )
        }

        val pagination = Pagination(
                page = page,
                perPage = perPage,
                totalPages = paginated.count(),
                totalEntries = paginated.sumBy { it.count() }
        )

        return LogResponse(
                entries = merged,
                pagination = pagination
        )
    }

    private fun fetchContactData(entries: List<Call>): Map<String, ContactData> {
        return try {
            contactDataProvider.getContactData(entries.map { it.number })
        } catch (e: Throwable) {
            err(e)
            emptyMap<String, ContactData>()
        }
    }
}