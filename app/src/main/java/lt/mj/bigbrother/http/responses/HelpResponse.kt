package lt.mj.bigbrother.http.responses

import lt.mj.bigbrother.http.data.Service

/**
 * @author Martynas Jurkus
 */
data class HelpResponse(
        val services: List<Service>
)