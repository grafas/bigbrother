package lt.mj.bigbrother.http.interactor

import lt.mj.bigbrother.http.responses.HelpResponse
import lt.mj.bigbrother.http.responses.LogResponse
import lt.mj.bigbrother.http.responses.StatusResponse

/**
 * @author Martynas Jurkus
 */
interface TraceDataInteractor {

    fun helpResponse(): HelpResponse

    fun statusResponse(): StatusResponse

    fun logResponse(page: Int, perPage: Int): LogResponse
}