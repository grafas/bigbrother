package lt.mj.bigbrother.http.data

import lt.mj.bigbrother.contacts.ContactData
import java.util.*

/**
 * @author Martynas Jurkus
 */
data class CallLog(
        val number: String,
        val callStart: Date = Date(),
        val duration: Long = 0L,
        val contactData: ContactData? = null,
        val timesQueried: Int = 0
)