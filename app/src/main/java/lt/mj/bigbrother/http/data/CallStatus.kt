package lt.mj.bigbrother.http.data

import lt.mj.bigbrother.contacts.ContactData

/**
 * @author Martynas Jurkus
 */
data class CallStatus(
        val number: String,
        val contactData: ContactData? = null
)