import android.content.Context
import lt.mj.bigbrother.BigBrotherApp
import lt.mj.bigbrother.dagger.ApplicationComponent

val Context.applicationComponent: ApplicationComponent
    get() = (this.applicationContext as BigBrotherApp).component