package lt.mj.bigbrother

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_NEW_OUTGOING_CALL
import android.telephony.TelephonyManager.*
import applicationComponent
import lt.mj.bigbrother.tracer.Tracer
import javax.inject.Inject


/**
 * @author Martynas Jurkus
 */
class PhoneStateReceiver : BroadcastReceiver() {

    @Inject
    lateinit var tracer: Tracer

    override fun onReceive(context: Context, intent: Intent) {
        if (!isValidReceiverAction(intent.action)) return

        context.applicationComponent.inject(this)

        if (intent.action == ACTION_NEW_OUTGOING_CALL) {
            tracer.newOutgoing(intent.extras.getString(Intent.EXTRA_PHONE_NUMBER))
        } else {
            val extraState = intent.extras.getString(EXTRA_STATE)
            val number = intent.extras.getString(EXTRA_INCOMING_NUMBER)

            val callState = when (extraState) {
                EXTRA_STATE_IDLE -> CALL_STATE_IDLE
                EXTRA_STATE_OFFHOOK -> CALL_STATE_OFFHOOK
                EXTRA_STATE_RINGING -> CALL_STATE_RINGING
                else -> 0
            }

            tracer.onCallStateChanged(callState, number)
        }
    }

    private fun isValidReceiverAction(action: String): Boolean {
        return action == ACTION_PHONE_STATE_CHANGED || action == ACTION_NEW_OUTGOING_CALL
    }
}
