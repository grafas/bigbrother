package lt.mj.bigbrother

import android.util.Log

/**
 * @author Martynas Jurkus
 */
const val TAG = "BigBrother"

fun Any.debug(message: String) {
    Log.d(TAG, message)
}

fun Any.err(e: Throwable) {
    Log.e(TAG, e.message, e)
}