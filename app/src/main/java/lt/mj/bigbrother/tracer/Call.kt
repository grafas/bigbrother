package lt.mj.bigbrother.tracer

/**
 * @author Martynas Jurkus
 */
data class Call(
        val number: String,
        val incoming: Boolean = false,
        val state: Int = 0,
        val callStart: Long = 0L,
        val duration: Long = 0L,
        val timesQueried: Int = 0
)