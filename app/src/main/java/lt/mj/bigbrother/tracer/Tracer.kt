package lt.mj.bigbrother.tracer

/**
 * @author Martynas Jurkus
 */
interface Tracer {

    val ongoingCall: Call?

    fun getCallLog(): List<Call>

    fun newOutgoing(phoneNumber: String)

    fun onCallStateChanged(callState: Int, number: String)

    fun incrementQueryCount()

    fun onServiceStop()
}