package lt.mj.bigbrother.tracer

import android.telephony.TelephonyManager.*


/**
 * @author Martynas Jurkus
 */
class TracerImpl : Tracer {

    private val callLog = mutableListOf<Call>()

    @Volatile
    override var ongoingCall: Call? = null

    override fun onServiceStop() {
        callLog.clear()
        ongoingCall = null
    }

    override fun incrementQueryCount() {
        val call = ongoingCall ?: return

        ongoingCall = call.copy(timesQueried = call.timesQueried + 1)
    }

    override fun getCallLog(): List<Call> {
        return callLog.toList()
    }

    override fun newOutgoing(phoneNumber: String) {
        if (ongoingCall != null) throw IllegalStateException("Ongoing call already started")

        ongoingCall = Call(
                number = phoneNumber,
                incoming = false,
                callStart = System.currentTimeMillis()
        )
    }

    override fun onCallStateChanged(callState: Int, number: String) {
        val call = getForNumber(number)

        if (call.state == callState) {
            return
        }

        when (callState) {
            CALL_STATE_RINGING -> {
                ongoingCall = call.copy(
                        state = CALL_STATE_RINGING
                )
            }
            CALL_STATE_OFFHOOK -> {
                ongoingCall = call.copy(
                        state = CALL_STATE_OFFHOOK
                )
            }
            CALL_STATE_IDLE -> {
                if (call.state == CALL_STATE_RINGING) {
                    onMissedCall(call)
                } else {
                    callEnded(call)
                }
            }
        }
    }

    private fun onMissedCall(call: Call) {
        ongoingCall = null
        callLog.add(call.copy(
                state = CALL_STATE_IDLE
        ))
    }

    private fun callEnded(call: Call) {
        ongoingCall = null
        callLog.add(call.copy(
                duration = System.currentTimeMillis() - call.callStart,
                state = CALL_STATE_IDLE
        ))
    }

    private fun getForNumber(number: String): Call {
        if (ongoingCall?.number == number) return ongoingCall!!

        return Call(
                number = number,
                incoming = true,
                callStart = System.currentTimeMillis()
        )
    }
}