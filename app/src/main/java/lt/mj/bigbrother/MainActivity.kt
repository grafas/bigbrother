package lt.mj.bigbrother

import android.Manifest.permission.*
import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.content.pm.PackageManager.*
import android.os.Bundle
import android.support.v4.app.ActivityCompat.requestPermissions
import android.support.v4.app.ActivityCompat.shouldShowRequestPermissionRationale
import android.support.v4.content.ContextCompat.checkSelfPermission
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import applicationComponent
import kotlinx.android.synthetic.main.activity_main.*
import lt.mj.bigbrother.http.BigBrotherServer
import lt.mj.bigbrother.tracer.Tracer
import javax.inject.Inject
import javax.inject.Named


class MainActivity : AppCompatActivity() {

    @field:[Inject Named("host")]
    lateinit var host: String
    @Inject
    lateinit var webServer: BigBrotherServer
    @Inject
    lateinit var tracer: Tracer

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        applicationComponent.inject(this)

        setContentView(R.layout.activity_main)

        service_toggle_button.setOnCheckedChangeListener { _, isChecked ->
            toggleBroadcastReceiver(isChecked)
        }

        updateServiceStatus()
    }

    private fun updateServiceStatus() {
        if (service_toggle_button.isChecked) {
            host_text.text = "$host:8081"
        } else {
            host_text.text = "Click below to start service"
        }
    }

    override fun onStart() {
        super.onStart()
        verifyPermissionsAreGranted()
    }

    private fun toggleBroadcastReceiver(enableService: Boolean) {
        val componentName = ComponentName(this, PhoneStateReceiver::class.java)

        val newComponentState = if (enableService) {
            webServer.start()
            COMPONENT_ENABLED_STATE_ENABLED
        } else {
            webServer.stop()
            tracer.onServiceStop()
            COMPONENT_ENABLED_STATE_DISABLED
        }

        packageManager.setComponentEnabledSetting(
                componentName,
                newComponentState,
                PackageManager.DONT_KILL_APP
        )

        updateServiceStatus()
    }

    private fun verifyPermissionsAreGranted(): Boolean {
        val permissionPhoneState = checkSelfPermission(this, READ_PHONE_STATE)
        val permissionOutgoingCall = checkSelfPermission(this, PROCESS_OUTGOING_CALLS)
        val permissionContacts = checkSelfPermission(this, READ_CONTACTS)

        val neededPermissions = mutableListOf<String>()
        if (permissionPhoneState != PERMISSION_GRANTED) {
            neededPermissions.add(READ_PHONE_STATE)
        }

        if (permissionOutgoingCall != PERMISSION_GRANTED) {
            neededPermissions.add(PROCESS_OUTGOING_CALLS)
        }

        if (permissionContacts != PERMISSION_GRANTED) {
            neededPermissions.add(READ_CONTACTS)
        }

        if (neededPermissions.isNotEmpty()) {
            requestPermissions(this, neededPermissions.toTypedArray(), REQUEST_ID_MULTIPLE_PERMISSIONS)
            return false
        }

        return true
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_ID_MULTIPLE_PERMISSIONS -> {
                val perms = mutableMapOf<String, Int>()
                // Initialize the map with both permissions
                perms[READ_PHONE_STATE] = PERMISSION_GRANTED
                perms[PROCESS_OUTGOING_CALLS] = PERMISSION_GRANTED
                perms[READ_CONTACTS] = PERMISSION_GRANTED

                if (grantResults.isNotEmpty()) {
                    for (i in permissions.indices) {
                        perms[permissions[i]] = grantResults[i]
                    }

                    val allPermissionsGranted = perms.all { it.value == PERMISSION_GRANTED }
                    if (allPermissionsGranted) {
                        //all is good!
                    } else {
                        if (shouldShowRequestPermissionRationale(this, READ_PHONE_STATE) ||
                                shouldShowRequestPermissionRationale(this, PROCESS_OUTGOING_CALLS) ||
                                shouldShowRequestPermissionRationale(this, READ_CONTACTS)) {
                            showDialogOK("Please give permissions :)",
                                    DialogInterface.OnClickListener { _, which ->
                                        when (which) {
                                            DialogInterface.BUTTON_POSITIVE -> {
                                                verifyPermissionsAreGranted()
                                            }
                                            DialogInterface.BUTTON_NEGATIVE -> {
                                                finish()
                                            }
                                        }
                                    })
                        } else {
                            Toast.makeText(
                                    this,
                                    "Go to settings and enable permissions",
                                    Toast.LENGTH_LONG
                            ).show()
                            finish()
                        }
                    }
                }
            }
        }
    }

    private fun showDialogOK(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show()
    }

    companion object {
        private const val REQUEST_ID_MULTIPLE_PERMISSIONS = 10_000
    }
}
